import os

from gauntlet import create_app

app = create_app('flask.cfg')

if __name__ == '__main__':
    os.environ['DOWNLOAD_ROOT'] = os.path.abspath(os.path.join('/', 'tmp'))
    app.run(host='0.0.0.0')
