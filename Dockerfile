FROM python:latest
MAINTAINER Gauntlet Developers "list.apacta2017gauntlet@credit-suisse.com"
COPY . /app
WORKDIR /app
RUN apt-get update && apt-get install -y git
RUN git --version
COPY ./config.yml /root/.kube/config
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin
RUN kubectl cluster-info
RUN curl -OL https://github.com/kubeless/kubeless/releases/download/v1.0.3/kubeless_linux-amd64.zip
RUN unzip kubeless_linux-amd64.zip
RUN chmod +x bundles/kubeless_linux-amd64/kubeless && mv bundles/kubeless_linux-amd64/kubeless /usr/local/bin/
RUN kubeless version
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["App.py"]