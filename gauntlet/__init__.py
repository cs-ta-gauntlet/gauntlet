import pathlib
import subprocess
import time
from unittest.mock import MagicMock

from flask import Flask
from kubernetes import client, config
from kubernetes.client import V1ObjectMeta
from kubernetes.client.rest import ApiException

from gauntlet.commands.deploy_function_command import DeployFunctionCommand
from gauntlet.runtime_configs.node_js_config import NodeJSConfiguration


def create_app(config_filename=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile(config_filename)
    register_blueprints(app)
    return app


def create_namespace(app, namespace):
    return app.post('/api/namespace/create', json={'namespace': namespace})


def register_blueprints(app):
    from gauntlet.controller import routes_blueprint
    app.register_blueprint(routes_blueprint.blueprint)


def remove_namespace_if_exists(namespace):
    config.load_kube_config()
    v1 = client.CoreV1Api()
    max_retries = 10
    retry_after_secs = 1
    try:
        v1_metadata_obj = V1ObjectMeta(name=namespace)
        body = client.V1Namespace(metadata=v1_metadata_obj)
        v1.delete_namespace(body=body, name=namespace)
        while max_retries != 0:
            v1.read_namespace(name=namespace)
            time.sleep(retry_after_secs)
            max_retries -= 1
        raise TimeoutError
    except ApiException:
        pass


def fake_command_with(result):
    return MagicMock(execute=MagicMock(return_value=result))


def remove_function(fn_name):  # very hacky, improve it after we have a proper delete function
    remove_func_command = "kubeless function delete {} -n briantan".format(fn_name)
    subprocess.Popen(remove_func_command, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
    time.sleep(15)


def deploy_some_function(namespace="briantan",
                         function_name="hello-world-js",
                         function_handler="handler.hello_world",
                         function_runtime="nodejs6"):
    DeployFunctionCommand(pathlib.Path(__file__).parent / "tests/resources/sample_node_project",
                          namespace,
                          function_runtime,
                          {"name": function_name,
                           "handler": function_handler},
                          NodeJSConfiguration()
                          ).execute()


def remove_http_trigger(fn_name):
    remove_http_trigger_command = "kubeless trigger http delete {} -n briantan".format(fn_name)
    subprocess.Popen(remove_http_trigger_command, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
