class NameSpaceController(object):

    def __init__(self, command_factory):
        self.command_factory = command_factory

    def create_namespace(self, namespace):
        return self.command_factory.new_namespace_creation_command(namespace).execute()
