import os

from flask import Blueprint, request, Flask, abort
from flask.json import jsonify

from gauntlet.controller.delete_controller import DeleteController
from gauntlet.controller.deploy_controller import DeployController
from gauntlet.controller.functions_controller import FunctionsController
from gauntlet.controller.namespace_controller import NameSpaceController
from gauntlet.factories.command_factory import CommandFactory
from gauntlet.mapper.runtime_config_mapper import RuntimeConfigMapper

blueprint = Blueprint('gauntlet_blueprint', __name__)

command_factory = CommandFactory()
runtime_config_mapper = RuntimeConfigMapper()

namespace_controller = NameSpaceController(command_factory)
deploy_controller = DeployController(command_factory, runtime_config_mapper)
delete_controller = DeleteController(command_factory)
functions_controller = FunctionsController(command_factory)


@blueprint.route("/api/deploy", methods=['POST'])
def deploy_function():
    payload = request.get_json(force=True, silent=True)
    if not payload or "url" not in payload:
        abort(400, "Url is not specified")
    results = deploy_controller.execute(payload=payload)
    result_json = jsonify(
        list(map(lambda result:
                 remove_nulls({
                     "function": result.function,
                     "message": result.message,
                     "backtrace": result.backtrace,
                     "api_url": result.api_url
                 }), results))
    )
    return result_json, RouteUtil.generate_status_code_with(results, "successful")


@blueprint.route("/api/delete", methods=['POST'])
def delete_function():
    payload = request.get_json(force=True, silent=True)
    if not payload or "namespace" not in payload or "function_name" not in payload:
        abort(400, "Invalid params")

    namespace, function_name = payload["namespace"], payload["function_name"]
    result = delete_controller.delete(namespace, function_name)
    if result.successful:
        return "Function deleted successfully", 200
    else:
        return "Error deleting function: %s" % result.message, 400


@blueprint.route("/api/ping", methods=['GET'])
def ping():
    return "pong"


@blueprint.route("/api/functions", methods=['GET'])
def list_functions_details():
    try:
        namespace = request.args['namespace']
        result = functions_controller.list_functions_details(namespace)
        return jsonify(
            list(map(lambda details: (details.to_dict()), result.functions_details))), 200 if result.successful else 400
    except:
        return "namespace required", 400


@blueprint.route("/api/namespace/create", methods=['POST'])
def create_name_space():
    payload = request.get_json(force=True, silent=True)
    result = namespace_controller.create_namespace(payload["namespace"])

    result_json = jsonify(
        remove_nulls({
            "message": result.message,
            "uid": result.uid,
            "url": result.url
        }))

    return result_json, 200 if result.successful else 400


def remove_nulls(d):
    return {k: v for k, v in d.items() if v is not None}


class RouteUtil(object):

    @staticmethod
    def generate_status_code_with(results, successful_key):
        if all(getattr(result, successful_key) for result in results):
            return 201
        elif any(getattr(result, successful_key) for result in results):
            return 200
        return 400


if __name__ == '__main__':
    os.environ['DOWNLOAD_ROOT'] = os.path.abspath(os.path.join('/', 'tmp'))
    print('DOWNLOAD_ROOT: %s' % os.environ['DOWNLOAD_ROOT'])
    app = Flask(__name__)
    app.register_blueprint(blueprint)
    app.run(port=5000, debug=True)
