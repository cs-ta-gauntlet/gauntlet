from gauntlet.models.delete_command_result import DeleteCommandResult


class DeleteController(object):
    def __init__(self, command_factory):
        self.command_factory = command_factory

    def delete(self, namespace, function_name):
        delete_cmd = self.command_factory.new_delete_command(namespace, function_name)
        try:
            delete_cmd.execute()
            return DeleteCommandResult(True)
        except Exception as e:
            return DeleteCommandResult(False, e.args[0].decode("utf-8"))
