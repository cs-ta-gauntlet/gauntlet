from gauntlet.exception.InvalidParameterException import InvalidParameterException


class DeployController(object):

    def __init__(self, command_factory, runtime_config_mapper):
        self.command_factory = command_factory
        self.runtime_config_mapper = runtime_config_mapper

    def execute(self, payload):
        url, checkout_to = self.validate(payload=payload)
        return self.download_and_deploy(url=url, checkout_to=checkout_to)

    @staticmethod
    def validate(payload):
        if "url" not in payload:
            raise InvalidParameterException("Url not specified")
        url = payload["url"]

        checkout_to = None
        if "branch" in payload and "tag" in payload:
            raise InvalidParameterException("Does not support both branch and tag together")
        elif "branch" in payload:
            checkout_to = payload["branch"]
        elif "tag" in payload:
            checkout_to = "tags/{}".format(payload["tag"])
        return url, checkout_to

    def download_and_deploy(self, url, checkout_to=None):
        deploy_results = []
        download_cmd = self.command_factory.new_download_command(url=url, checkout_to=checkout_to)
        download_cmd.execute()
        deploy_cfg = download_cmd.deploy_cfg
        namespace = deploy_cfg.namespace
        runtime = deploy_cfg.runtime

        for function in deploy_cfg.functions:
            runtime_config = self.runtime_config_mapper.get_config_for(runtime)
            action = self.get_deploy_action(namespace, function["name"])
            deploy_cmd = self.command_factory.new_deploy_command(download_cmd.download_dir, namespace, runtime,
                                                                 function, runtime_config, action)
            result = deploy_cmd.execute()
            deploy_results.append(result)

        return deploy_results

    def get_deploy_action(self, namespace, function_name):
        check_function_exists_command = self.command_factory.new_check_function_exists_command(namespace, function_name)
        function_exists = check_function_exists_command.execute()
        if function_exists:
            return 'update'
        else:
            return 'deploy'
