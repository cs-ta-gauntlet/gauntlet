class FunctionsController(object):

    def __init__(self, command_factory):
        self.command_factory = command_factory

    def list_functions_details(self, namespace):
        return self.command_factory.new_list_functions_details_command(namespace).execute()
