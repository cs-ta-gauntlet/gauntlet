import subprocess

from gauntlet.commands.i_command import ICommand
from gauntlet.models.deploy_command_result import DeployCommandResult

DEFAULT_HOSTNAME = 'kubelessfun.ga'
FUNCTION_COMMANDS = ['deploy', 'update']
#TODO: shall we have another command for update?


class DeployFunctionCommand(ICommand):

    def __init__(self, project_path, namespace, runtime, function, runtime_config, action="deploy"):
        self.project_path = project_path
        self.namespace = namespace
        self.runtime = runtime
        self.function = function
        self.runtime_config = runtime_config
        if not self.is_valid_action(action):
            raise Exception('Invalid Action type: {}'.format(action))
        self.action = action
        self.deploy_cmd = self.construct_deploy_cmd()

    def is_valid_action(self, action):
        return action in FUNCTION_COMMANDS

    def construct_deploy_cmd(self):
        navigate_to_project_cmd = self.get_navigate_to_project_cmd()
        kubeless_deploy_cmd = self.get_deploy_cmd()
        if self.action is 'deploy':
            http_trigger_creation_cmd = self.get_http_trigger_creation_cmd()
            return self.join_cmd(navigate_to_project_cmd, kubeless_deploy_cmd, http_trigger_creation_cmd)
        else:
            return self.join_cmd(navigate_to_project_cmd, kubeless_deploy_cmd)

    def get_navigate_to_project_cmd(self):
        return "cd {}".format(self.project_path)

    def get_deploy_cmd(self):
        function_name = self.function['name']
        function_handler = self.function['handler']
        function_file = function_handler.split('.')[0]
        return "kubeless function {} {} " \
               "--runtime {} " \
               "--dependencies {} " \
               "--handler {} " \
               "--namespace {} " \
               "--from-file {}.{}".format(self.action, function_name,
                                          self.runtime,
                                          self.runtime_config.dependency_file,
                                          function_handler,
                                          self.namespace,
                                          function_file,
                                          self.runtime_config.file_appendix)

    def get_http_trigger_creation_cmd(self):
        function_name = self.function['name']
        return "kubeless trigger http create {}" \
               " --function-name {}" \
               " --path {}" \
               " --hostname {}" \
               " --namespace {}".format(function_name,
                                        function_name,
                                        self.namespace + "/" + function_name,
                                        DEFAULT_HOSTNAME,
                                        self.namespace)

    def execute(self):
        cmd = subprocess.Popen(self.deploy_cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
        output, exit_code = cmd.communicate(), cmd.returncode
        print(output)
        url = self.construct_function_api(hostname=DEFAULT_HOSTNAME,
                                          namespace=self.namespace,
                                          function_name=self.function['name'])
        if exit_code is not 0:
            return DeployCommandResult(function=self.function['name'],
                                       successful=False,
                                       message="Failed to deploy function",
                                       backtrace=str(output[0]))
        return DeployCommandResult(function=self.function['name'],
                                   successful=True,
                                   api_url=url,
                                   message="Function deployed successfully")

    @staticmethod
    def construct_function_api(hostname, namespace, function_name):
        return hostname + "/" + namespace + "/" + function_name
