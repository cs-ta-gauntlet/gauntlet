import subprocess
from gauntlet.commands.i_command import ICommand


class CheckFunctionExistsCommand(ICommand):
    def __init__(self, namespace, function_name):
        self.namespace = namespace
        self.function_name = function_name
        self.command = self.construct_cmd_string()

    def construct_cmd_string(self):
        return 'kubeless function ls {} -n {}'.format(self.function_name, self.namespace)

    def execute(self):
        cmd = subprocess.Popen(self.command, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
        output, exit_code = cmd.communicate(), cmd.returncode
        if exit_code is 0:
            return True
        else:
            return False
