import json
import subprocess

from gauntlet.commands.i_command import ICommand
from gauntlet.models.function_details import FunctionDetails
from gauntlet.models.list_functions_details_command_result import ListFunctionsDetailsCommandResult


class ListFunctionsDetailsCommand(ICommand):
    def __init__(self, namespace):
        self.namespace = namespace
        self.console_cmd = self.construct_console_cmd()

    def get_console_cmd(self):
        return self.console_cmd

    def construct_console_cmd(self):
        return "kubeless function ls --namespace {} --out json".format(self.namespace)

    def execute(self):
        cmd = subprocess.Popen(self.console_cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
        output, exit_code = cmd.communicate(), cmd.returncode
        if exit_code == 0:
            functions_details = list(map(lambda kubeless_list_result: FunctionDetails().with_details_from(kubeless_list_result),
                     json.loads(output[0].decode('UTF-8'))))
            return ListFunctionsDetailsCommandResult(successful=True, functions_details=functions_details)
        else:
            return ListFunctionsDetailsCommandResult(successful=False, backtrace=str(output[0]))

