import subprocess


# TODO: implement ICommand


class DeleteFunctionCommand(object):
    def __init__(self, namespace, function_name):
        self.namespace = namespace
        self.function_name = function_name
        self.command = self.construct_cmd_string()

    def construct_cmd_string(self):
        return 'kubeless function delete {} -n {}'.format(self.function_name, self.namespace)

    def execute(self):
        cmd = subprocess.Popen(self.command, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
        output, exit_code = cmd.communicate(), cmd.returncode
        if exit_code is not 0:
            raise Exception(output[0] if len(output) > 0 else "Delete function command failed")
