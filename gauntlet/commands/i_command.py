from abc import abstractmethod


class ICommand:

    @abstractmethod
    def execute(self):
        raise NotImplementedError

    @staticmethod
    def join_cmd(*commands):
        return " && ".join(commands)
