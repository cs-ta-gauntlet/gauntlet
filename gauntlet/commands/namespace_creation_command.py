import re

from kubernetes import client, config
from kubernetes.client import V1ObjectMeta
from kubernetes.client.rest import ApiException

from gauntlet.commands.i_command import ICommand
from gauntlet.models.namespace_creation_command_result import NamespaceCreationCommandResult


class NamespaceCreationCommand(ICommand):
    def __init__(self, namespace):
        config.load_kube_config()

        self.namespace = namespace
        api_client = client.ApiClient()
        self.v1 = client.CoreV1Api(api_client)

    def execute(self):
        return self.create_namespace() if self.validate_namespace_format() \
            else NamespaceCreationCommandResult(successful=False,
                                                message="Invalid format for namespace '{}'".format(self.namespace))

    def create_namespace(self):
        v1_metadata_obj = V1ObjectMeta(name=self.namespace)
        body = client.V1Namespace(metadata=v1_metadata_obj)

        try:
            response = self.v1.create_namespace(body=body)
            return NamespaceCreationCommandResult(successful=True,
                                                  uid=response.metadata.uid,
                                                  url=response.metadata.self_link)
        except ApiException:
            return NamespaceCreationCommandResult(successful=False, message="Namespace '{}' already exists".format(self.namespace))

    def validate_namespace_format(self):
        return True if re.match(r"^[a-z0-9]([-a-z0-9]*[a-z0-9])+$", self.namespace) else False


