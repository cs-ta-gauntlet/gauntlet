import os
import subprocess
from datetime import datetime

from gauntlet.commands.i_command import ICommand
from gauntlet.models.deploy_config import DeployConfig


class DownloadCommand(ICommand):
    def __init__(self, url, checkout_to=None):
        self.download_dir = os.path.join(os.environ["DOWNLOAD_ROOT"],
                                         "tmp_repo_%s" % datetime.now().strftime('%Y%m%d%H%M%S'))
        self.url = url
        self.checkout_to = checkout_to
        self.command = self.construct_cmd_string()
        self.deploy_cfg = None

    def execute(self):
        self.download_source()
        self.parse_deploy_config()

    def download_source(self):
        cmd = subprocess.Popen(self.command, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
        output, exit_code = cmd.communicate(), cmd.returncode
        if exit_code is not 0:
            raise Exception(output[0] if len(output) > 0 else "Boom!")

    def parse_deploy_config(self):
        self.deploy_cfg = DeployConfig(os.path.join(self.download_dir, "deploy.yml"))

    def construct_cmd_string(self):
        clone_cmd = self.construct_clone_cmd_string()
        if self.checkout_to is not None:
            return self.join_cmd(clone_cmd, self.construct_cd_cmd_string(), self.construct_checkout_cmd_string())
        return clone_cmd

    def construct_clone_cmd_string(self):
        return "git clone {} {}".format(self.url, self.download_dir)

    def construct_cd_cmd_string(self):
        return "cd {}".format(self.download_dir)

    def construct_checkout_cmd_string(self):
        return "git checkout {}".format(self.checkout_to)
