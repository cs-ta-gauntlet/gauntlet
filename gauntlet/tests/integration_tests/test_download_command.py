import json
import os
import unittest

from gauntlet.commands.download_command import DownloadCommand


class TestDownloadCommand(unittest.TestCase):
    test_path = 'test'
    repo_url = "https://gitlab.com/kennethckc/sample_node_project"
    repo_with_branch_url = "https://gitlab.com/cs-ta-gauntlet/demo-nodejs6-branch"
    branch = "feature1"
    tag = "tags/v2"

    @classmethod
    def setUpClass(cls):
        cls.test_path = os.path.abspath(os.path.join('/', 'function-repos'))
        os.environ['DOWNLOAD_ROOT'] = cls.test_path

    def test_can_download_source_from_valid_git_repository(self):
        download_cmd = DownloadCommand(self.repo_url)
        download_cmd.execute()

        self.assertEqual(download_cmd.deploy_cfg.runtime, "nodejs6")
        self.assertEqual(download_cmd.deploy_cfg.namespace, "briantan")

        expected_config = [
            {"name": "hello-world", "handler": "handler.hello_world"},
            {"name": "hello-new-world", "handler": "new_handler.hello_new_world"}
        ]
        expected_dump = json.dumps(expected_config, sort_keys=True)
        actual_dump = json.dumps(download_cmd.deploy_cfg.functions, sort_keys=True)
        self.assertEqual(expected_dump, actual_dump)

    def test_can_download_source_from_gitlab_with_branch_specified(self):
        download_cmd = DownloadCommand(self.repo_with_branch_url, self.branch)
        download_cmd.execute()

        self.assertEqual(download_cmd.deploy_cfg.runtime, "nodejs6")
        self.assertEqual(download_cmd.deploy_cfg.namespace, "function-demo")

        expected_config = [
            {"name": "branch", "handler": "handler.branch_name"}
        ]
        expected_dump = json.dumps(expected_config, sort_keys=True)
        actual_dump = json.dumps(download_cmd.deploy_cfg.functions, sort_keys=True)
        self.assertEqual(expected_dump, actual_dump)

    def test_can_download_source_from_gitlab_with_tag_specified(self):
        download_cmd = DownloadCommand(self.repo_with_branch_url, self.tag)
        download_cmd.execute()

        self.assertEqual(download_cmd.deploy_cfg.runtime, "nodejs6")
        self.assertEqual(download_cmd.deploy_cfg.namespace, "function-demo")

        expected_config = [
            {"name": "tagv2", "handler": "handler.branch_name"}
        ]
        expected_dump = json.dumps(expected_config, sort_keys=True)
        actual_dump = json.dumps(download_cmd.deploy_cfg.functions, sort_keys=True)
        self.assertEqual(expected_dump, actual_dump)
