import unittest

from gauntlet import remove_namespace_if_exists
from gauntlet.commands.namespace_creation_command import NamespaceCreationCommand


class NamespaceCreationCommandTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.namespace = "namespace-creation-cmd-test"

    def test_successfully_create_namespace_with_non_existing_namespace(self):
        remove_namespace_if_exists(self.namespace)
        self.namespace_creation_command = NamespaceCreationCommand(self.namespace)
        namespace_creation_result = self.namespace_creation_command.execute()

        self.assertEqual(True, namespace_creation_result.successful)
        self.assertNotEqual(namespace_creation_result.uid, None)
        self.assertNotEqual(namespace_creation_result.url, None)

    def test_unsuccessfully_create_namespace_with_existing_namespace(self):
        remove_namespace_if_exists(self.namespace)
        self.namespace_creation_command = NamespaceCreationCommand(self.namespace)
        namespace_creation_result = self.namespace_creation_command.execute()
        self.assertEqual(True, namespace_creation_result.successful)

        namespace_creation_result = self.namespace_creation_command.execute()

        self.assertEqual(False, namespace_creation_result.successful)
        self.assertEqual(namespace_creation_result.uid, None)
        self.assertEqual(namespace_creation_result.url, None)


if __name__ == '__main__':
    unittest.main()
