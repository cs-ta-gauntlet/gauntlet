import pathlib
import subprocess
import time
import unittest

from gauntlet.commands.delete_function_command import DeleteFunctionCommand
from gauntlet.commands.deploy_function_command import DeployFunctionCommand
from gauntlet.runtime_configs.node_js_config import NodeJSConfiguration


class TestDeleteCommand(unittest.TestCase):
    def test_can_delete_deployed_function(self):
        deploy_command = DeployFunctionCommand(pathlib.Path(__file__).parent / "../resources/sample_node_project",
                                               "briantan",
                                               'nodejs6',
                                               {"name": "hello-world-js-test",
                                                "handler": "handler.hello_world"},
                                               NodeJSConfiguration()
                                               )

        deploy_command.execute()

        is_deployed = False
        while not is_deployed:
            time.sleep(10)
            check_deploy_fn_process = subprocess.Popen("kubeless function list -n briantan | grep hello-world-js-test",
                                                       stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
            deploy_output = str(check_deploy_fn_process.communicate()[0], "utf8")
            deploy_exit_code = check_deploy_fn_process.returncode

            self.assertTrue(deploy_exit_code == 0)
            self.assertTrue("hello-world-js-test" in deploy_output)
            is_deployed = "READY" in deploy_output and "NOT READY" not in deploy_output

        delete_cmd = DeleteFunctionCommand("briantan", "hello-world-js-test")
        delete_cmd.execute()
        time.sleep(30)  # to wait for function to be deleted completely
        result = subprocess.call("kubeless function list -n briantan | grep hello-world-js-test", shell=True)

        self.assertTrue(result == 1)
