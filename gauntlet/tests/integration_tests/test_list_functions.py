import unittest

from gauntlet import remove_function, deploy_some_function
from gauntlet.commands.list_functions_command import ListFunctionsDetailsCommand


class ListFunctionsDetailsIntegrationTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.namespace = "briantan"
        cls.function_name = "hello-world-js"
        cls.function_handler = "handler.hello_world"
        cls.function_runtime = "nodejs6"
        remove_function(cls.function_name)
        deploy_some_function(namespace=cls.namespace,
                             function_name=cls.function_name,
                             function_handler=cls.function_handler,
                             function_runtime=cls.function_runtime)

    def test_can_list_functions(self):
        result = ListFunctionsDetailsCommand(self.namespace).execute()
        targeted_detail = self.find_details_by_func_name(self.function_name, result.functions_details)

        self.assertEqual(True, result.successful)
        self.assertEqual(targeted_detail.name, self.function_name)
        self.assertEqual(targeted_detail.handler, self.function_handler)
        self.assertEqual(targeted_detail.runtime, self.function_runtime)
        self.assertIsNotNone(targeted_detail.creation_timestamp)

    @staticmethod
    def find_details_by_func_name(function_name, function_details):
        return next(details for details in function_details if details.name == function_name)



if __name__ == '__main__':
    unittest.main()
