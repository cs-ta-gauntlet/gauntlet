import pathlib
import unittest

from gauntlet import remove_function
from gauntlet.commands.deploy_function_command import DeployFunctionCommand
from gauntlet.runtime_configs.java_config import JavaConfiguration
from gauntlet.runtime_configs.node_js_config import NodeJSConfiguration
from gauntlet.runtime_configs.python_config import PythonConfiguration


class DeployFunctionIntegrationTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.namespace = "briantan"
        cls.fn = pathlib.Path(__file__).parent

    def test_can_successfully_deploy_node_project(self):
        remove_function("hello-world-js")

        deploy_command = DeployFunctionCommand(self.fn / "../resources/sample_node_project",
                                               self.namespace,
                                               'nodejs6',
                                               {"name": "hello-world-js",
                                                "handler": "handler.hello_world"},
                                               NodeJSConfiguration()
                                               )
        result = deploy_command.execute()

        self.assertEqual(True, result.successful)
        self.assertEqual("hello-world-js", result.function)

    def test_can_successfully_update_node_project(self):
        remove_function("hello-world-js")

        deploy_command = DeployFunctionCommand(self.fn / "../resources/sample_node_project",
                                               self.namespace,
                                               'nodejs6',
                                               {"name": "hello-world-js",
                                                "handler": "handler.hello_world"},
                                               NodeJSConfiguration(), "deploy")

        update_command = DeployFunctionCommand(self.fn / "../resources/sample_node_project",
                                               self.namespace,
                                               'nodejs6',
                                               {"name": "hello-world-js",
                                                "handler": "handler.hello_world"},
                                               NodeJSConfiguration(), "update")
        deploy_command.execute()
        result = update_command.execute()
        print(result)
        self.assertEqual(True, result.successful)
        self.assertEqual("hello-world-js", result.function)

    def test_can_successfully_deploy_nodejs8_project(self):
        remove_function("hello-world-js")

        deploy_command = DeployFunctionCommand(self.fn / "../resources/sample_node_project",
                                               self.namespace,
                                               'nodejs8',
                                               {"name": "hello-world-js",
                                                "handler": "handler.hello_world"},
                                               NodeJSConfiguration()
                                               )
        result = deploy_command.execute()

        self.assertEqual(True, result.successful)
        self.assertEqual("hello-world-js", result.function)

    def test_can_successfully_deploy_python_project(self):
        remove_function("hello-world-py")

        deploy_command = DeployFunctionCommand(self.fn / "../resources/sample_python_project",
                                               self.namespace,
                                               'python2.7',
                                               {"name": "hello-world-py",
                                                "handler": "handler.hello_world"},
                                               PythonConfiguration()
                                               )
        result = deploy_command.execute()

        self.assertEqual(True, result.successful)
        self.assertEqual("hello-world-py", result.function)

    def test_can_successfully_deploy_java_project(self):
        remove_function("hello-world-java")
        deploy_command = DeployFunctionCommand(self.fn / "../resources/sample_java_project",
                                               self.namespace,
                                               'java1.8',
                                               {"name": "hello-world-java",
                                                "handler": "handler.helloWorld"},
                                               JavaConfiguration()
                                               )

        result = deploy_command.execute()

        self.assertEqual(True, result.successful)
        self.assertEqual("hello-world-java", result.function)

    def test_can_successfully_deploy_python34_project(self):
        remove_function("hello-world-py3.4")
        deploy_command = DeployFunctionCommand(self.fn / "../resources/sample_python3_4_project",
                                               self.namespace,
                                               'python3.4',
                                               {"name": "hello-world-py3.4",
                                                "handler": "handler.hello_world"},
                                               PythonConfiguration()
                                               )

        result = deploy_command.execute()

        self.assertEqual(True, result.successful)
        self.assertEqual("hello-world-py3.4", result.function)

    def test_can_successfully_deploy_python36_project(self):
        remove_function("hello-world-py3.6")
        deploy_command = DeployFunctionCommand(self.fn / "../resources/sample_python3_6_project",
                                               self.namespace,
                                               'python3.6',
                                               {"name": "hello-world-py3.6",
                                                "handler": "handler.hello_world"},
                                               PythonConfiguration()
                                               )
        result = deploy_command.execute()

        self.assertEqual(True, result.successful)
        self.assertEqual("hello-world-py3.6", result.function)


if __name__ == '__main__':
    unittest.main()
