import pathlib
import subprocess
import unittest

from App import app
from gauntlet.commands.deploy_function_command import DeployFunctionCommand
from gauntlet.runtime_configs.node_js_config import NodeJSConfiguration


class DeleteTest(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True
        deploy_command = DeployFunctionCommand(
            pathlib.Path(__file__).parent / "../resources/sample_node_project",
            "briantan",
            'nodejs6',
            {
                "name": "hello-world-js",
                "handler": "handler.hello_world"
            },
            NodeJSConfiguration()
        )
        deploy_command.execute()
        result = subprocess.call("kubeless function list -n briantan | grep hello-world-js", shell=True)
        assert result == 0

    def test_can_get_successful_message_if_delete_successful(self):
        response = self.app.post('/api/delete', json={"namespace": "briantan", "function_name": "hello-world-js"})

        self.assertEqual(response.data.decode("utf-8"), "Function deleted successfully")
        self.assertEqual(200, response.status_code)

    def test_can_get_error_if_delete_unsuccessful(self):
        self.app.post('/api/delete', json={"namespace": "briantan", "function_name": "hello-world-js"})

        response = self.app.post('/api/delete', json={"namespace": "briantan", "function_name": "hello-world-js"})

        self.assertEqual(400, response.status_code)
