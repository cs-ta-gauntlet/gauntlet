import json
import unittest

from App import app
from gauntlet import remove_function, deploy_some_function


class ListFunctionsTest(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True
        self.namespace = "briantan"
        self.function_name = "hello-world"
        self.function_handler = "handler.hello_world"
        self.function_runtime = "nodejs6"

        remove_function(self.function_name)
        deploy_some_function(namespace=self.namespace,
                             function_name=self.function_name,
                             function_handler=self.function_handler,
                             function_runtime=self.function_runtime)

    def test_can_list_functions_result_by_namespace(self):
        response = self.list_functions(self.namespace)

        json_response = json.loads(response.data)
        targeted_detail = self.find_details_by_func_name(self.function_name, json_response)

        self.assertEqual(200, response.status_code)
        self.assertEqual(targeted_detail['name'], self.function_name)
        self.assertEqual(targeted_detail['handler'], self.function_handler)
        self.assertEqual(targeted_detail['runtime'], self.function_runtime)
        self.assertIsNotNone(targeted_detail['creation_timestamp'])

    @staticmethod
    def find_details_by_func_name(function_name, function_details):
        return next(details for details in function_details if details['name'] == function_name)

    def list_functions(self, namespace):
        return self.app.get('/api/functions?namespace={}'.format(namespace))
