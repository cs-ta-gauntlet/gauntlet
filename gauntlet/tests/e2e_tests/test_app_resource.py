import unittest

from App import app


class AppTest(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_ping_api(self):
        response = self.app.get('/api/ping')
        self.assertEqual(200, response.status_code)
        self.assertEqual(b'pong', response.data)
