import json
import os
import unittest

from App import app
from gauntlet import remove_function, remove_http_trigger


class DeployTest(unittest.TestCase):

    def setUp(self):
        os.environ['DOWNLOAD_ROOT'] = os.path.abspath(os.path.join('test', 'tmp'))
        self.app = app.test_client()
        self.app.testing = True
        self.TEST_PACKAGE_URL = "https://gitlab.com/kennethckc/sample_node_project"
        self.remove_functions_and_triggers()

    @staticmethod
    def remove_functions_and_triggers():
        remove_function("hello-world")
        remove_function("hello-new-world")
        remove_http_trigger("hello-world")
        remove_http_trigger("hello-new-world")

    def test_can_get_successful_message_if_deploy_successful(self):
        response = self.deploy(self.TEST_PACKAGE_URL)

        json_response = json.loads(response.data)

        self.assertEqual(201, response.status_code)
        self.assertEqual(json_response,
            [
                {
                    "message": "Function deployed successfully",
                    "function": "hello-world",
                    "api_url": "kubelessfun.ga/briantan/hello-world",
                },
                {
                    "message": "Function deployed successfully",
                    "function": "hello-new-world",
                    "api_url": "kubelessfun.ga/briantan/hello-new-world",
                }
            ]
        )

    def test_can_redeploy_function(self):
        self.deploy(self.TEST_PACKAGE_URL)
        response = self.deploy(self.TEST_PACKAGE_URL)
        json_response = json.loads(response.data)

        self.assertEqual(200, response.status_code)
        self.assertEqual(json_response,
                         [
                             {
                                 "message": "Function deployed successfully",
                                 "function": "hello-world",
                                 "api_url": "kubelessfun.ga/briantan/hello-world",
                             },
                             {
                                 "message": "Function deployed successfully",
                                 "function": "hello-new-world",
                                 "api_url": "kubelessfun.ga/briantan/hello-new-world",
                             }
                         ]
                         )

    def test_can_get_partial_successful_status_code(self):
        self.deploy(self.TEST_PACKAGE_URL)
        remove_function("hello-world")
        response = self.deploy(self.TEST_PACKAGE_URL)
        json_response = json.loads(response.data)

        self.assertEqual(200, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertEqual(["hello-world",
                          "hello-new-world"],
                         self.get_flattened("function", json_response))
        self.assertEqual("kubelessfun.ga/briantan/hello-world",
                         json_response[0]["api_url"])
        self.assertTrue("api_url" not in json_response[1])

        self.assertEqual(["Function deployed successfully",
                          "Failed to deploy function"],
                         self.get_flattened("message", json_response))
        self.assertIsNotNone(json_response[1]["backtrace"])

    def get_flattened(self, n, json_response):
        return list(map(lambda d: d[n], json_response))

    def deploy(self, url):
        return self.app.post('/api/deploy', json={'url': url})
