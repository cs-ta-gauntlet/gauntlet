import unittest

from flask import json

from App import app
from gauntlet import remove_namespace_if_exists, create_namespace


class NamespaceCreationTest(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True
        self.TEST_NS = "test-some-funny-ns"
        remove_namespace_if_exists(self.TEST_NS)

    def test_namespace_creation_can_get_success_payload(self):
        response = create_namespace(self.app, self.TEST_NS)

        response_json = json.loads(response.data)

        self.assertEqual(200, response.status_code)
        self.assertEqual('application/json', response.content_type)
        self.assertIsNotNone(response_json['url'])
        self.assertIsNotNone(response_json['uid'])

    def test_namespace_creation_with_existing_namespace_can_get_unsuccessful_payload(self):
        create_namespace(self.app, self.TEST_NS)

        response = create_namespace(self.app, self.TEST_NS)
        response_json = json.loads(response.data)

        self.assertEqual(400, response.status_code)
        self.assertEqual("Namespace '{}' already exists".format(self.TEST_NS), response_json['message'])

    def test_namespace_creation_with_invalid_namespace_can_get_unsuccessful_payload(self):
        invalid_namespace = "invalidNamespace"

        response = create_namespace(self.app, invalid_namespace)
        response_json = json.loads(response.data)

        self.assertEqual(400, response.status_code)
        self.assertEqual("Invalid format for namespace '{}'".format(invalid_namespace), response_json['message'])
