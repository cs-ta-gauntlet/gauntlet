import os
import subprocess
import unittest
from unittest import mock

from gauntlet.commands.download_command import DownloadCommand


class TestDownloadCommand(unittest.TestCase):
    test_path = os.path.abspath(os.path.join('/', 'tmp'))
    test_url = "www.example.com"
    branch_name = "abc"

    @classmethod
    def setUpClass(cls):
        os.environ['DOWNLOAD_ROOT'] = cls.test_path
        if not os.path.exists(cls.test_path):
            os.makedirs(cls.test_path)

    def test_can_throw_error_when_download_with_invalid_url(self):
        invalid_url = "this is an invalid url"
        with self.assertRaises(Exception):
            DownloadCommand(invalid_url).download_source()

    def test_can_construct_cmd_with_no_branch_and_tag(self):
        download_cmd = DownloadCommand(url=self.test_url)
        constructed_cmd_string = download_cmd.construct_cmd_string()

        self.assertTrue("git clone {} {}".format(self.test_url, download_cmd.download_dir) == constructed_cmd_string)

    @mock.patch("subprocess.Popen")
    def test_executes_correct_command_with_no_branch_and_tag(self, mock_subprocess):
        download_cmd = DownloadCommand(url=self.test_url)
        instance = mock_subprocess.return_value
        instance.returncode = 0
        download_cmd.download_source()
        mock_subprocess.assert_called_with("git clone {} {}".format(self.test_url, download_cmd.download_dir),
                                           stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

    def test_can_construct_cmd_with_branch_specified(self):
        download_cmd = DownloadCommand(url=self.test_url, checkout_to=self.branch_name)
        constructed_cmd_string = download_cmd.construct_cmd_string()

        self.assertTrue("git clone {} {} && cd {} && git checkout {}".format(self.test_url,
                                                                             download_cmd.download_dir,
                                                                             download_cmd.download_dir,
                                                                             self.branch_name) == constructed_cmd_string)

    @mock.patch("subprocess.Popen")
    def test_executes_correct_command_with_branch_specified(self, mock_subprocess):
        download_cmd = DownloadCommand(url=self.test_url, checkout_to=self.branch_name)
        instance = mock_subprocess.return_value
        instance.returncode = 0
        download_cmd.download_source()
        mock_subprocess.assert_called_with("git clone {} {} && cd {} && git checkout {}".format(self.test_url,
                                                                                                download_cmd.download_dir,
                                                                                                download_cmd.download_dir,
                                                                                                self.branch_name),
                                           stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

    def test_can_construct_cd_cmd(self):
        download_cmd = DownloadCommand(url=self.test_url, checkout_to=self.branch_name)
        constructed_cmd_string = download_cmd.construct_cd_cmd_string()
        assert constructed_cmd_string == "cd {}".format(download_cmd.download_dir)

    def test_can_join_cmd(self):
        download_cmd = DownloadCommand(url=self.test_url)
        joined_cmd = download_cmd.join_cmd("ls", "pwd", "ps aux | grep something")
        assert joined_cmd == "ls && pwd && ps aux | grep something"
