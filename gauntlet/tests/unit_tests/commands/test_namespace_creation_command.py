import unittest

from gauntlet.commands.namespace_creation_command import NamespaceCreationCommand


class TestNamespaceCreationCommand(unittest.TestCase):
    valid_namespace_param_list = ["creation-test", "creation-test-v1"]
    invalid_namespace_param_list = ["CreationTest"]

    def test_namespace_format_validation_successful(self):
        for namespace in self.valid_namespace_param_list:
            with self.subTest():
                namespace_command_creation = NamespaceCreationCommand(namespace)
                valid = namespace_command_creation.validate_namespace_format()
                self.assertEqual(True, valid)

    def test_namespace_format_validation_unsuccessful(self):
        for namespace in self.invalid_namespace_param_list:
            with self.subTest():
                namespace_command_creation = NamespaceCreationCommand(namespace)
                valid = namespace_command_creation.validate_namespace_format()
                self.assertEqual(False, valid)


if __name__ == '__main__':
    unittest.main()
