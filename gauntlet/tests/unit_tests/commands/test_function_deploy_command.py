import subprocess
import unittest.mock

from mock import MagicMock

from gauntlet.commands.deploy_function_command import DeployFunctionCommand


class TestFunctionDeployCommand(unittest.TestCase):

    def setUp(self):
        self.some_project_path = "K://some_project_dir"
        self.function = {"name": "some-test-function", "handler": "stupid.handler"}
        self.runtime_config = MagicMock(dependency_file="package.json", file_appendix="js")
        self.deploy_function_cmd = DeployFunctionCommand(self.some_project_path,
                                                         "some-namespace",
                                                         "some-runtime",
                                                         self.function,
                                                         self.runtime_config,
                                                         "deploy"
                                                         )

    def test_can_return_false_if_invalid_action_is_passed(self):
        assert self.deploy_function_cmd.is_valid_action("invalid") == False

    def test_can_return_true_if_valid_action_is_passed(self):
        assert self.deploy_function_cmd.is_valid_action("update") == True
        assert self.deploy_function_cmd.is_valid_action("deploy") == True

    def test_can_get_navigate_to_project_cmd(self):
        navigate_command = self.deploy_function_cmd.get_navigate_to_project_cmd()
        assert navigate_command == "cd {}".format(self.some_project_path)

    def test_can_get_deploy_function_cmd(self):
        deploy_function_command = self.deploy_function_cmd.get_deploy_cmd()
        assert deploy_function_command == "kubeless function deploy {} " \
                                          "--runtime {} " \
                                          "--dependencies package.json " \
                                          "--handler {} " \
                                          "--namespace {} " \
                                          "--from-file {}.js".format("some-test-function",
                                                                     "some-runtime",
                                                                     "stupid.handler",
                                                                     "some-namespace",
                                                                     "stupid")

    def test_can_get_create_http_trigger_cmd(self):
        create_http_trigger_cmd = self.deploy_function_cmd.get_http_trigger_creation_cmd()
        assert create_http_trigger_cmd == "kubeless trigger http create {}" \
                                          " --function-name {}" \
                                          " --path {}" \
                                          " --hostname {}" \
                                          " --namespace {}".format("some-test-function",
                                                                   "some-test-function",
                                                                   "some-namespace/some-test-function",
                                                                   "kubelessfun.ga",
                                                                   "some-namespace")

    def test_can_construct_correct_deploy_command(self):
        commands = []
        commands.append("cd {}".format(self.some_project_path))
        deploy_cmd = "kubeless function deploy {} " \
                     "--runtime {} " \
                     "--dependencies package.json " \
                     "--handler {} " \
                     "--namespace {} " \
                     "--from-file {}.js".format("some-test-function",
                                                "some-runtime",
                                                "stupid.handler",
                                                "some-namespace",
                                                "stupid")
        commands.append(deploy_cmd)
        http_trigger_creation_cmd = "kubeless trigger http create {}" \
                                    " --function-name {}" \
                                    " --path {}" \
                                    " --hostname {}" \
                                    " --namespace {}".format("some-test-function",
                                                             "some-test-function",
                                                             "some-namespace/some-test-function",
                                                             "kubelessfun.ga",
                                                             "some-namespace")
        commands.append(http_trigger_creation_cmd)
        expected_cmd = " && ".join(commands)
        assert self.deploy_function_cmd.deploy_cmd == expected_cmd

    @unittest.mock.patch("subprocess.Popen")
    def test_will_run_deploy_command_during_execute(self, mock_subprocess):
        instance = mock_subprocess.return_value
        instance.returncode = 0
        self.deploy_function_cmd.deploy_cmd = "some-command"
        self.deploy_function_cmd.execute()
        mock_subprocess.assert_called_with("some-command", stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)

    def test_can_construct_function_api_url(self):
        function_api = self.deploy_function_cmd.construct_function_api(hostname="example.com",
                                                                       namespace="testing",
                                                                       function_name="some_function")
        assert function_api == "example.com/testing/some_function"

    def test_can_join_cmd(self):
        joined_cmd = self.deploy_function_cmd.join_cmd("ls", "pwd", "ps aux | grep something")
        assert joined_cmd == "ls && pwd && ps aux | grep something"


if __name__ == '__main__':
    unittest.main()
