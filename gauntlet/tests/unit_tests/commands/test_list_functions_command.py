import unittest.mock

from gauntlet.commands.list_functions_command import ListFunctionsDetailsCommand


class TestListFunctionsDetailsCommand(unittest.TestCase):

    def test_can_construct_correct_list_functions_command(self):
        namespace = "someNameSpace"
        expected_cmd = "kubeless function ls --namespace {} --out json".format(namespace)
        assert ListFunctionsDetailsCommand(namespace).get_console_cmd() == expected_cmd

if __name__ == '__main__':
    unittest.main()
