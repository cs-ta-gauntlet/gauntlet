import subprocess
import unittest
from unittest import mock

from gauntlet.commands.check_function_exists_command import CheckFunctionExistsCommand


class TestCheckFunctionExistsCommand(unittest.TestCase):
    def setUp(self):
        self.namespace = "test-check-function-namespace"
        self.function_name = "test-check-function-name"

    def test_can_construct_check_function_exists_command_given_namespace_and_function(self):
        check_function_exists_cmd = CheckFunctionExistsCommand(self.namespace, self.function_name)
        constructed_cmd_string = check_function_exists_cmd.construct_cmd_string()

        self.assertTrue(
            'kubeless function ls {} -n {}'.format(self.function_name, self.namespace) in constructed_cmd_string)

    @unittest.mock.patch("subprocess.Popen")
    def test_will_return_true_if_command_executes_successfully(self, mock_subprocess):
        check_function_exists_cmd = CheckFunctionExistsCommand(self.namespace, self.function_name)
        instance = mock_subprocess.return_value
        instance.returncode = 0
        result = check_function_exists_cmd.execute()
        self.assertEqual(True, result)

    @unittest.mock.patch("subprocess.Popen")
    def test_will_return_false_if_command_returns_unsuccessful_exit_code(self, mock_subprocess):
        check_function_exists_cmd = CheckFunctionExistsCommand(self.namespace, self.function_name)
        instance = mock_subprocess.return_value
        instance.returncode = -1
        result = check_function_exists_cmd.execute()
        self.assertEqual(False, result)

    @unittest.mock.patch("subprocess.Popen")
    def test_will_run_check_function_exists_command_during_execute(self, mock_subprocess):
        check_function_exists_command = CheckFunctionExistsCommand(self.namespace, self.function_name)
        instance = mock_subprocess.return_value
        instance.returncode = 0
        check_function_exists_command.command = "test-exists-command"
        check_function_exists_command.execute()
        mock_subprocess.assert_called_with("test-exists-command", stderr=subprocess.STDOUT, stdout=subprocess.PIPE,
                                           shell=True)


