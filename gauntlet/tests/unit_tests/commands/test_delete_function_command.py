import os
import subprocess
import unittest
from unittest import mock

from gauntlet.commands.delete_function_command import DeleteFunctionCommand


class TestDeleteFunctionCommand(unittest.TestCase):
    def setUp(self):
        self.namespace = "test-delete-function-namespace"
        self.function_name = "test-delete-function-name"

    def test_can_construct_delete_command_given_namespace_and_function(self):
        delete_function_cmd = DeleteFunctionCommand(self.namespace, self.function_name)
        constructed_cmd_string = delete_function_cmd.construct_cmd_string()

        self.assertTrue('kubeless function delete {} -n {}'.format(self.function_name, self.namespace) in constructed_cmd_string)

    @unittest.mock.patch("subprocess.Popen")
    def test_will_run_delete_function_command_during_execute(self, mock_subprocess):
        delete_function_cmd = DeleteFunctionCommand(self.namespace, self.function_name)
        instance = mock_subprocess.return_value
        instance.returncode = 0
        delete_function_cmd.command = "test-delete-command"
        delete_function_cmd.execute()
        mock_subprocess.assert_called_with("test-delete-command", stderr=subprocess.STDOUT, stdout=subprocess.PIPE, shell=True)
