import unittest

from gauntlet.mapper.runtime_config_mapper import RuntimeConfigMapper


class RuntimeConfigMapperTest(unittest.TestCase):

    def setUp(self):
        self.mapper = RuntimeConfigMapper()

    def test_can_get_node_js_configuration_for_runtime_equal_nodejs6(self):
        config = self.mapper.get_config_for("nodejs6")

        self.assertEqual("package.json", config.dependency_file)
        self.assertEqual("js", config.file_appendix)

    def test_can_get_python_configuration_for_runtime_equal_python27(self):
        config = self.mapper.get_config_for("python2.7")

        self.assertEqual("requirements.txt", config.dependency_file)
        self.assertEqual("py", config.file_appendix)

    def test_can_get_python_configuration_for_runtime_equal_python34(self):
        config = self.mapper.get_config_for("python3.4")

        self.assertEqual("requirements.txt", config.dependency_file)
        self.assertEqual("py", config.file_appendix)

    def test_can_get_python_configuration_for_runtime_equal_python36(self):
        config = self.mapper.get_config_for("python3.6")

        self.assertEqual("requirements.txt", config.dependency_file)
        self.assertEqual("py", config.file_appendix)

    def test_can_get_java_configuration_for_runtime_equal_java18(self):
        config = self.mapper.get_config_for("java1.8")

        self.assertEqual("pom.xml", config.dependency_file)
        self.assertEqual("java", config.file_appendix)
