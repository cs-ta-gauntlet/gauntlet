import unittest

from mock import MagicMock

from gauntlet import fake_command_with
from gauntlet.controller.namespace_controller import NameSpaceController
from gauntlet.models.namespace_creation_command_result import NamespaceCreationCommandResult


class NameSpaceControllerTest(unittest.TestCase):

    def test_call_create_namespace_then_create_namespace(self):
        successful_result = NamespaceCreationCommandResult(successful=True, uid="some-uid", url="some-url")
        fake_command_factory = MagicMock(
            new_namespace_creation_command=MagicMock(
                return_value=fake_command_with(successful_result)))
        ns_controller = NameSpaceController(fake_command_factory)

        self.assertEqual(successful_result, ns_controller.create_namespace("some-namespace"))
