import unittest
from unittest.mock import MagicMock

from gauntlet.controller.routes_blueprint import RouteUtil


class TestRouteUtil(unittest.TestCase):

    def test_can_return_201_if_all_results_are_successful(self):
        results = [MagicMock(successful=True), MagicMock(successful=True)]
        self.assertEqual(201, RouteUtil.generate_status_code_with(results, 'successful'))

    def test_can_return_200_if_some_results_are_successful_and_some_are_not(self):
        results = [MagicMock(successful=True), MagicMock(successful=False)]
        self.assertEqual(200, RouteUtil.generate_status_code_with(results, 'successful'))

    def test_can_return_400_if_all_results_are_not_successful(self):
        results = [MagicMock(successful=False), MagicMock(successful=False)]
        self.assertEqual(400, RouteUtil.generate_status_code_with(results, 'successful'))
