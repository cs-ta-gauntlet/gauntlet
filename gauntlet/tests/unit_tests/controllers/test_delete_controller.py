import unittest

from mock import MagicMock

from gauntlet.controller.delete_controller import DeleteController


class DeleteControllerTest(unittest.TestCase):

    def setUp(self):
        self.fake_factory = MagicMock()
        self.delete_controller = DeleteController(self.fake_factory)

    @staticmethod
    def fake_delete_command(namespace, function_name):
        return MagicMock(namespace=namespace, function_name=function_name,
                         execute=MagicMock())

    def test_will_delete_function(self):
        namespace, function_name = "someNamespace", "someFunction"
        self.fake_factory.new_delete_command = MagicMock(
            return_value=self.fake_delete_command(namespace, function_name))

        self.delete_controller.delete(namespace, function_name)
        self.fake_factory.new_delete_command.assert_any_call(namespace, function_name)


if __name__ == '__main__':
    unittest.main()
