import unittest

from mock import MagicMock

from gauntlet import fake_command_with
from gauntlet.controller.deploy_controller import DeployController
from gauntlet.exception.InvalidParameterException import InvalidParameterException
from gauntlet.models.deploy_command_result import DeployCommandResult


class DeployControllerTest(unittest.TestCase):

    def setUp(self):
        self.download_dir = "some_dir"
        self.deploy_config = MagicMock(runtime="some-runtime", namespace="some-namespace")
        self.fake_factory = MagicMock()
        self.runtime_config = MagicMock()
        self.fake_runtime_config_mapper = MagicMock(get_config_for=MagicMock(return_value=self.runtime_config))
        self.deploy_controller = DeployController(self.fake_factory, self.fake_runtime_config_mapper)

    @staticmethod
    def fake_download_command(result, download_dir, deploy_cfg):
        return MagicMock(download_dir=download_dir, deploy_cfg=deploy_cfg,
                         execute=MagicMock(return_value=result))

    @staticmethod
    def fake_check_function_exists_command(result, namespace, function_name):
        return MagicMock(namespace=namespace, function_name=function_name,
                         execute=MagicMock(return_value=result))

    def test_will_deploy_multiple_functions(self):
        deploy_config = self.deploy_config
        functions_to_deploy = deploy_config.functions = \
            [{"name": "some-test-function", "handler": "stupid.handler"},
             {"name": "another-test-function", "handler": "another_stupid.handler"}]
        self.fake_factory.new_download_command = MagicMock(
            return_value=self.fake_download_command(True, self.download_dir, deploy_config))
        deploy_results = [DeployCommandResult(successful=True, function="some-test-function"),
                          DeployCommandResult(successful=True, function="another-test-function")]
        self.fake_factory.new_deploy_command = MagicMock(
            side_effect=list(map(fake_command_with, deploy_results)))
        self.deploy_controller.get_deploy_action = MagicMock(return_value='deploy')

        result = self.deploy_controller.download_and_deploy("some_unused_url")

        for idx, function in enumerate(functions_to_deploy):
            self.fake_factory.new_deploy_command.assert_any_call(self.download_dir,
                                                                 deploy_config.namespace,
                                                                 deploy_config.runtime,
                                                                 function,
                                                                 self.runtime_config, 'deploy')
            self.assertEqual(result[idx], deploy_results[idx])

    def test_get_deploy_action_return_update_if_function_exists(self):
        namespace = 'test-namespace'
        function_name = 'test-function'
        self.fake_factory.new_check_function_exists_command = MagicMock(
            return_value=self.fake_check_function_exists_command(True, namespace, function_name))
        result = self.deploy_controller.get_deploy_action(namespace, function_name)
        self.assertEqual('update', result)

    def test_get_deploy_action_return_deploy_if_function_does_not_exist(self):
        namespace = 'test-namespace'
        function_name = 'test-function'
        self.fake_factory.new_check_function_exists_command = MagicMock(
            return_value=self.fake_check_function_exists_command(False, namespace, function_name))
        result = self.deploy_controller.get_deploy_action(namespace, function_name)
        self.assertEqual('deploy', result)

    def test_can_validate_payload_with_only_url(self):
        payload = {"url": "http://example.com"}
        url, checkout_to = self.deploy_controller.validate(payload)
        assert url == "http://example.com"
        assert checkout_to is None

    def test_can_throw_error_when_validate_payload_with_no_url(self):
        payload = {}
        self.assertRaises(InvalidParameterException, self.deploy_controller.validate, payload)

    def test_can_validate_payload_with_url_and_branch(self):
        payload = {"url": "http://example.com", "branch": "abc"}
        url, checkout_to = self.deploy_controller.validate(payload)
        assert url == "http://example.com"
        assert checkout_to == "abc"

    def test_can_validate_payload_with_url_and_tag(self):
        payload = {"url": "http://example.com", "tag": "0.0.1"}
        url, checkout_to = self.deploy_controller.validate(payload)
        assert url == "http://example.com"
        assert checkout_to == "tags/0.0.1"

    def test_can_throw_exception_when_validate_payload_with_url_and_tag_and_branch(self):
        payload = {"url": "http://example.com", "tag": "0.0.1", "branch": "abc"}
        self.assertRaises(InvalidParameterException, self.deploy_controller.validate, payload)


if __name__ == '__main__':
    unittest.main()
