import unittest

from gauntlet.models.function_details import FunctionDetails


class TestFunctionDetail(unittest.TestCase):

    def test_can_construct_result_from_kubeless_output(self):
        function_detail = FunctionDetails()
        function_detail.construct_details_from(test_payload)
        self.assertEqual(function_detail.name, "hello-new-world")
        self.assertEqual(function_detail.runtime, "nodejs6")
        self.assertEqual(function_detail.handler, "new_handler.hello_new_world")
        self.assertEqual(function_detail.creation_timestamp, "2019-05-09T10:57:21Z")



if __name__ == '__main__':
    unittest.main()


test_payload = {
    "kind": "Function",
    "apiVersion": "kubeless.io/v1beta1",
    "metadata": {
        "name": "hello-new-world",
        "namespace": "briantan",
        "selfLink": "/apis/kubeless.io/v1beta1/namespaces/briantan/functions/hello-new-world",
        "uid": "388859c1-7249-11e9-ad7e-161c3adcdcbc",
        "resourceVersion": "7701155",
        "generation": 2,
        "creationTimestamp": "2019-05-09T10:57:21Z",
        "labels": {
            "created-by": "kubeless",
            "function": "hello-new-world"
        },
        "finalizers": [
            "kubeless.io/function"
        ]
    },
    "spec": {
        "handler": "new_handler.hello_new_world",
        "function": "\"use strict\";\r\n\r\nconst _ = require(\"lodash\");\r\n\r\nmodule.exports = {\r\n  hello_new_world(event, context) {\r\n    return \"Hello New World! This is a nodejs project.\";\r\n  },\r\n};\r\n",
        "function-content-type": "text",
        "checksum": "sha256:bda014f5cab7d3364a1e2c80e9320ad60e9d2fa90cb2cec8fecbb4a00c1f028d",
        "runtime": "nodejs6",
        "timeout": "180",
        "deps": "{\r\n  \"name\": \"kubeless-nodejs\",\r\n  \"version\": \"1.0.0\",\r\n  \"description\": \"Example function for serverless kubeless\",\r\n  \"dependencies\": {\r\n    \"lodash\": \"^4.1.0\",\r\n    \"serverless-kubeless\": \"^0.4.0\"\r\n  },\r\n  \"devDependencies\": {\r\n    \"@babel/core\": \"^7.3.4\",\r\n    \"@babel/preset-env\": \"^7.3.4\",\r\n    \"babel-jest\": \"^24.5.0\",\r\n    \"jest\": \"^24.5.0 \"\r\n  },\r\n  \"scripts\": {\r\n    \"test\": \"jest\"\r\n  },\r\n  \"keywords\": [\r\n    \"serverless\",\r\n    \"kubeless\"\r\n  ],\r\n  \"author\": \"The Kubeless Authors\",\r\n  \"license\": \"Apache-2.0\"\r\n}\r\n",
        "deployment": {
            "metadata": {
                "creationTimestamp": None
            },
            "spec": {
                "template": {
                    "metadata": {
                        "creationTimestamp": None
                    },
                    "spec": {
                        "containers": [
                            {
                                "name": "",
                                "resources": {},
                                "imagePullPolicy": "Always"
                            }
                        ]
                    }
                },
                "strategy": {}
            },
            "status": {}
        },
        "service": {
            "ports": [
                {
                    "name": "http-function-port",
                    "protocol": "TCP",
                    "port": 8080,
                    "targetPort": 8080
                }
            ],
            "selector": {
                "created-by": "kubeless",
                "function": "hello-new-world"
            },
            "type": "ClusterIP"
        },
        "horizontalPodAutoscaler": {
            "metadata": {
                "creationTimestamp": None
            },
            "spec": {
                "scaleTargetRef": {
                    "kind": "",
                    "name": ""
                },
                "maxReplicas": 0
            },
            "status": {
                "currentReplicas": 0,
                "desiredReplicas": 0,
                "currentMetrics": None,
                "conditions": None
            }
        }
    }
}