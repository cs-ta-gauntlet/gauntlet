import unittest
import os
import os.path

from gauntlet.models.deploy_config import DeployConfig


class TestReadDeployYml(unittest.TestCase):
    def test_can_read_deploy_config(self):
        file_path = os.path.join(os.getcwd(),
                                 'gauntlet', 'tests', 'resources',
                                 'sample_node_project', 'deploy.yml'
                                 )
        config = DeployConfig(file_path)
        assert config.runtime == "nodejs6"
        assert config.namespace == "briantan"
        assert config.functions == [{
            "name": "hello-world",
            "handler": "handler.hello_world"},
            {
                "name": "hello-new-world",
                "handler": "second_handler.hello_new_world"
            }]


if __name__ == '__main__':
    unittest.main()
