from gauntlet.commands.check_function_exists_command import CheckFunctionExistsCommand
from gauntlet.commands.delete_function_command import DeleteFunctionCommand
from gauntlet.commands.deploy_function_command import DeployFunctionCommand
from gauntlet.commands.download_command import DownloadCommand
from gauntlet.commands.list_functions_command import ListFunctionsDetailsCommand
from gauntlet.commands.namespace_creation_command import NamespaceCreationCommand


class CommandFactory:
    @staticmethod
    def new_download_command(url, checkout_to):
        return DownloadCommand(url, checkout_to)

    @staticmethod
    def new_deploy_command(project_path, namespace, runtime, function, runtime_config, action):
        return DeployFunctionCommand(project_path, namespace, runtime, function, runtime_config, action)

    @staticmethod
    def new_check_function_exists_command(namespace, function_name):
        return CheckFunctionExistsCommand(namespace, function_name)

    @staticmethod
    def new_namespace_creation_command(namespace):
        return NamespaceCreationCommand(namespace)

    @staticmethod
    def new_delete_command(namespace, function_name):
        return DeleteFunctionCommand(namespace, function_name)

    @staticmethod
    def new_list_functions_details_command(namespace):
        return ListFunctionsDetailsCommand(namespace)
