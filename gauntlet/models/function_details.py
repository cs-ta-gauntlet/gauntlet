class FunctionDetails:

    def __init__(self, name=None, handler=None, runtime=None, creation_timestamp=None):
        self.name = name
        self.runtime = runtime
        self.handler = handler
        # self.status = status
        self.creation_timestamp = creation_timestamp

    def construct_details_from(self, kubeless_dictionary):
        self.name = kubeless_dictionary["metadata"]["name"]
        self.runtime = kubeless_dictionary["spec"]["runtime"]
        self.handler = kubeless_dictionary["spec"]["handler"]
        self.creation_timestamp = kubeless_dictionary["metadata"]["creationTimestamp"]

    def with_details_from(self, kubeless_dictionary):
        self.construct_details_from(kubeless_dictionary)
        return self

    def to_dict(self):
        return self.__dict__
