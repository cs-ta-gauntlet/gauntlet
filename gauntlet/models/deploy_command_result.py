class DeployCommandResult:
    def __init__(self, successful=None, function=None, message=None, backtrace=None, api_url=None):
        self.function = function
        self.successful = successful
        self.message = message
        self.backtrace = backtrace
        self.api_url = api_url
