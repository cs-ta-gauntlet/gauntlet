from io import open
import yaml


class DeployConfig:

    def __init__(self, file_name):
        stream = open(file_name, mode="r", encoding="utf-8")
        loaded_yml = yaml.load(stream=stream, Loader=yaml.Loader)
        self.runtime = loaded_yml.get('runtime')
        self.namespace = loaded_yml.get('namespace')
        self.functions = loaded_yml.get('functions')
