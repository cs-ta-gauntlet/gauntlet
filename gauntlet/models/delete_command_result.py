class DeleteCommandResult:
    def __init__(self, successful=False, message=None, backtrace=None):
        self.successful = successful
        self.message = message
        self.backtrace = backtrace
