class NamespaceCreationCommandResult:
    def __init__(self, successful=False, uid=None, url=None, message=None):
        self.successful = successful
        self.uid = uid
        self.url = url
        self.message = message
