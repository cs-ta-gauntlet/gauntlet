class ListFunctionsDetailsCommandResult:
    def __init__(self, successful=None, backtrace=None, functions_details=None):
        self.successful = successful
        self.backtrace = backtrace
        self.functions_details = functions_details

