from gauntlet.runtime_configs.java_config import JavaConfiguration
from gauntlet.runtime_configs.node_js_config import NodeJSConfiguration
from gauntlet.runtime_configs.python_config import PythonConfiguration


class RuntimeConfigMapper(object):

    def __init__(self):
        self.configs = {
            "nodejs6": NodeJSConfiguration,
            "nodejs8": NodeJSConfiguration,
            "python2.7": PythonConfiguration,
            "python3.4": PythonConfiguration,
            "python3.6": PythonConfiguration,
            "java1.8": JavaConfiguration
        }

    def get_config_for(self, runtime):
        return self.configs[runtime]()
